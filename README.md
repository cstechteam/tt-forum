# Tech-Team forum

## Setup
An admin account will created upon the initial migration with the password
"admin". This project does not use the django admin app - all administration is
done directly on the forum.

## Users

### Admin
Unless 'signals.py' is edited, the admin account will have the username and
password 'admin'. This account should not be used for day-to-day activities.

### Registering
Anyone can register an account on the forum but will not be able to post until
ownership of at least one email address can be confirmed.

### Account settings
All account settings can be found by clicking the "Account" button at the
top-right of the screen.

### User administration
User admin will usually be done at board-level - if a user has at least a
moderator account a "User management" button will appear on their boards. From
this board they can give individual users certain permissions by searching their
username from the given form, or, they can manage user permissions in bulk by
uploading a CSV file (using ',' as delimiters) and give all the users a
specified permissions level (the board must be exclusive to change user's access
level, see the user access section below).

## User access
Each board can be either exclusive or non-exclusive. When a board is
non-exclusive any user can join and a user will be given access as soon as they
view the board. Threads or the board itself cannot be made private within a
non-exclusive board as this would not make sense. If a board is exclusive, users
must be explicitly given access either through their profile page or by using
the CSV bulk-management tool.

## Board maintenance
The admin account and board-owners can change the board's settings from the
admin panel at the top of the board. Settings are toggled by clicking their
respective buttons. Depending on the user's level extra buttons might be shown
next to each thread that allows managing threads (sticky, lock, etc.)

## Board/thread/post deletion
The "delete" button next to posts/threads will only mark posts or threads as
deleted and will not remove them from the database. They can be restored again
by clicking the same button. Moderators, board-owners and admin will still be
able to see the deleted posts/threads, but normal users will not. The admin can
delete boards in a similar fashion but this is a permanent deletion and cannot
be undone. The admin has the extra option of permanently deleting posts and
threads by using the "Destroy" button. This action cannot be undone and will
remove the thread/post from the database.

## Deployment
1. Install all the dependencies with pip
2. Migrate the models at least once to create the admin profile
3. Set all the relevant database and email-server settings in
   'tt_forum/settings.py'
