from django.contrib import admin
from django.urls import include, path

urlpatterns = [
        path('', include('forum.urls')),
        path('accounts/', include('django.contrib.auth.urls')),
]
