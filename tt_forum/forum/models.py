from django.db import models
from django.contrib.auth.models import User
from django.conf import settings
from django.core.files.storage import FileSystemStorage

attachment_storage = FileSystemStorage(location=settings.ATTACHMENT_ROOT)

def pp_filename(instance, filename):
    return "pp/{}/{}".format(instance.user.id, filename)

class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    join = models.DateField(auto_now_add=True)
    posts = models.IntegerField(default=0)
    admin = models.BooleanField(default=False)
    prf_pic = models.ImageField(upload_to=pp_filename, blank=True)

    notf_qr = models.BooleanField(default=True)
    notf_auto = models.BooleanField(default=True)

    new_em = models.EmailField(null=True, blank=True)
    cnf_em = models.BooleanField(default=False)
    token_salt = models.PositiveSmallIntegerField(default=0)

    # By generating reproducible tokens no tokens have to be stored
    # in an extra database
    def gen_token(self, no_up=False):
        import hashlib
        h = hashlib.sha256()
        # Password is added to string so that server-only reproducible
        # data (password salt etc.) is included in the hash, preventing
        # the user from reproducing the token themselves
        #
        # The token_salt is merely a counter to prevent successive tokens
        # being the same
        if not no_up:
            self.token_salt += 1
            self.save()
        s = self.user.username + "|" + \
            self.user.email + "|" + \
            self.user.password + "|" + \
            str(self.token_salt)
        h.update(s.encode('utf-8'))
        return h.hexdigest()

    def val_token(self, token):
        return self.gen_token(no_up=True) == token

    def __str__(self):
        return self.user.username

class Board(models.Model):
    name = models.CharField(max_length=150, null=True)
    last = models.DateTimeField(blank=True, null=True)

    private = models.BooleanField(default=False)
    exclusive = models.BooleanField(default=False)
    allow_files = models.BooleanField(default=True)
    users = models.ManyToManyField(Profile, blank=True, related_name='access_boards')
    banned_users = models.ManyToManyField(Profile, blank=True, related_name='banned_boards')

    owners = models.ManyToManyField(Profile, blank=True, related_name='owned_boards')
    moderators = models.ManyToManyField(Profile, blank=True, related_name='mod_boards')

    subbed_users = models.ManyToManyField(Profile, blank=True, related_name='sub_boards')

    def __str__(self):
        return self.name

class Thread(models.Model):
    title = models.CharField(max_length=150)
    last = models.DateTimeField(auto_now_add=True)

    sticky = models.BooleanField(default=False)
    locked = models.BooleanField(default=False)
    deleted = models.BooleanField(default=False)

    private = models.BooleanField(default=False)
    parent = models.ForeignKey(Board, on_delete=models.CASCADE, related_name='children')

    subbed_users = models.ManyToManyField(Profile, blank=True, related_name='sub_threads')

    def __str__(self):
        return self.parent.name + " - " + self.title

class Post(models.Model):
    user = models.ForeignKey(Profile, null=True, on_delete=models.SET_NULL)
    time = models.DateTimeField(auto_now_add=True)

    quote = models.ForeignKey('self', null=True, blank=True, on_delete=models.SET_NULL)
    text = models.TextField()

    sticky = models.BooleanField(default=False)
    deleted = models.BooleanField(default=False)

    parent = models.ForeignKey(Thread, on_delete=models.CASCADE, related_name='children')

    def __str__(self):
        return self.parent.title + " - " + self.user.user.username + " - " + str(self.time)

    def get_md(self):
        from markdown import markdown
        import bleach
        import re

        t = bleach.clean(self.text)
        t = markdown(t,
                extensions=['markdown.extensions.codehilite',],
                extension_configs = {
                    'markdown.extensions.codehilite' : {
                        'linenums': True
                        }
                    })
        t = bleach.linkify(t)
        t = re.sub(r'@(?P<id>[0-9]+)@', '<a href="/post/\g<id>">reference</a>', t)
        return t

def attachment_filename(instance, filename):
    from time import gmtime, strftime
    s = "{}/%Y/%m/%d/{}"
    s = strftime(s, gmtime())
    return s.format(instance.parent.user.id, filename)

class Attachment(models.Model):
    attachment = models.FileField(blank=True, upload_to=attachment_filename, storage=attachment_storage)
    parent = models.ForeignKey(Post, on_delete=models.CASCADE, related_name='attachments')
    name = models.CharField(max_length=150)
