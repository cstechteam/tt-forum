from django.urls import path, re_path

from django.conf import settings
from django.conf.urls.static import static

from . import views

app_name = 'forum'
urlpatterns = [
        path('', views.index, name='index'),
        path('board/<int:obj_id>/<int:page_id>/', views.board, name='board'),
        path('thread/<int:obj_id>/<int:page_id>/', views.thread, name='thread'),
        path('post/<int:obj_id>', views.post, name='post'),
        path('file/<int:obj_id>', views.file, name='file'),
        path('profile/<int:obj_id>', views.profile, name='profile'),

        path('new_board/', views.new_board, name='new_board'),
        path('new_thread/<int:obj_id>/', views.new_thread, name='new_thread'),
        path('new_post/<int:obj_id>/', views.new_post, name='new_post'),

        path('board_exc/<int:obj_id>/<int:page_id>/', views.board_exc, name='board_exc'),
        path('board_private/<int:obj_id>/<int:page_id>/', views.board_private, name='board_private'),
        path('board_files/<int:obj_id>/<int:page_id>', views.board_files, name='board_files'),
        path('board_delete/<int:obj_id>/', views.board_delete, name='board_delete'),
        path('board_sub/<int:obj_id>/<int:page_id>/', views.board_sub, name='board_sub'),

        path('thread_sticky/<int:obj_id>/<int:page_id>', views.thread_sticky, name='thread_sticky'),
        path('thread_delete/<int:obj_id>/<int:page_id>', views.thread_delete, name='thread_delete'),
        path('thread_lock/<int:obj_id>/<int:page_id>', views.thread_lock, name='thread_lock'),
        path('thread_private/<int:obj_id>/<int:page_id>', views.thread_private, name='thread_private'),
        path('thread_destroy/<int:obj_id>/<int:page_id>/', views.thread_destroy, name='thread_destroy'),
        path('thread_sub/<int:obj_id>/<int:page_id>/', views.thread_sub, name='thread_sub'),

        path('post_sticky/<int:obj_id>/', views.post_sticky, name='post_sticky'),
        path('post_delete/<int:obj_id>/', views.post_delete, name='post_delete'),
        path('post_destroy/<int:obj_id>/<int:page_id>/', views.post_destroy, name='post_destroy'),

        path('profile_own/<int:obj_id>/<int:profile_id>/', views.profile_own, name='profile_own'),
        path('profile_mod/<int:obj_id>/<int:profile_id>/', views.profile_mod, name='profile_mod'),
        path('profile_acc/<int:obj_id>/<int:profile_id>/', views.profile_acc, name='profile_acc'),
        path('profile_ban/<int:obj_id>/<int:profile_id>/', views.profile_ban, name='profile_ban'),

        path('profile_search/', views.profile_search, name='profile_search'),
        path('profile_bulk/<int:obj_id>/<int:page_id>', views.profile_bulk, name='profile_bulk'),

        path('register/', views.register, name='register'),
        path('register_success/', views.register_success, name='register_success'),
        path('my_account/', views.my_account, name='my_account'),

        re_path(r'^confirm_email/(?P<token>[a-z0-9]{64})/$', views.confirm_email, name='confirm_email'),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
