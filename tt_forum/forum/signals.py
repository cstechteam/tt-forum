def populate_models(sender, **kwargs):
    from django.contrib.auth.models import User, Group, Permission
    from django.contrib.contenttypes.models import ContentType
    from .models import Profile, Board

    if Profile.objects.count() > 0:
        return

    # Create admin User
    admin = User.objects.create_user(
            username='admin',
            password='admin',
            is_superuser=True,
            is_staff=True,)
    admin.save()

    # Create admin Profile
    prf = Profile(user=admin, admin=True, cnf_em=True)
    prf.save();
