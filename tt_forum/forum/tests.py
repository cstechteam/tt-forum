from django.test import TestCase, RequestFactory
from django.contrib.auth.models import User, AnonymousUser
from .models import *
from . import views

from django.urls import reverse
from django.core.exceptions import PermissionDenied

def create_board(self, **kwargs):
    if not hasattr(self, 'boards'):
        self.boards = {}
    board = Board.objects.create(
            name="board_" + str(create_board.cnt),
            **kwargs
            )
    create_board.cnt += 1
    self.boards[board.name] = board
    return board
create_board.cnt = 0

def create_thread(self, board, **kwargs):
    if not hasattr(self, 'threads'):
        self.threads = {}
    thread = Thread.objects.create(
            title=board.name + "_thread_" + str(create_thread.cnt),
            parent=board,
            **kwargs
            )
    create_thread.cnt += 1
    self.threads[thread.title] = thread
    return thread
create_thread.cnt = 0

def create_post(self, thread, user, **kwargs):
    if not hasattr(self, 'posts'):
        self.posts = {}
    post = Post.objects.create(
            user=user,
            parent=thread,
            text = thread.title + "_" + str(create_post.cnt),
            **kwargs
            )
    create_post.cnt += 1
    self.posts[post.id] = post
    return post
create_post.cnt = 0

def create_attachment(self, post, **kwargs):
    if not hasattr(self, 'attachments'):
        self.attachments = {}
    attachment = Attachment.objects.create(
            parent=post,
            **kwargs
            )
    self.attachments[attachment.id] = attachment
    return attachment

def create_profile(self, cnf_em=True, **kwargs):
    if not hasattr(self, 'profiles'):
        self.profiles = {}
    username = "user" + str(create_profile.cnt)
    create_profile.cnt += 1
    user = User.objects.create_user(username=username,
            password=username,
            email= username + '@email.com' if cnf_em else '',
            )
    profile = Profile.objects.create(user=user,
            admin=False,
            cnf_em=cnf_em,
            new_em = username + '@email.com' if not cnf_em else '',
            **kwargs
            )
    self.profiles[profile.id] = profile
    return profile
create_profile.cnt = 0

def setup(self):
    self.factory = RequestFactory()

def setup_request(self, url, user):
    request = self.factory.get(url)
    request.session = {}
    request.user = user

    return request

def run_objects(self, user_args, board_args, thread_args, status_code=200, post=False):
    setup(self)

    # Create user
    user = None
    ua = None
    if user_args:
        if user_args.get('admin', False):
            user = User.objects.get(username='admin')
        else:
            if user_args.get('usertype', False):
                ua = user_args['usertype']
                del user_args['usertype']
            user = create_profile(self, **user_args).user
    else:
        user = AnonymousUser()

    # Test board
    ass = board_args['ass']
    del board_args['ass']
    board = create_board(self, **board_args)
    if ua:
        if ua == 'no-access':
            pass
        elif ua == 'access':
            board.users.add(user.profile)
        elif ua == 'banned':
            board.banned_users.add(user.profile)

    request = setup_request(self, reverse('forum:board', args=(board.id, 0)), user)
    try:
        response = views.board(request, obj_id=board.id, page_id=0)
        self.assertEqual(response.status_code, status_code)
    except PermissionDenied:
        self.assertTrue(ass)
    else:
        self.assertFalse(ass)

    # Test thread
    ass = thread_args['ass']
    del thread_args['ass']
    thread = create_thread(self, board, **thread_args)
    try:
        if post:
            request = setup_request(self, reverse('forum:new_post', args=(thread.id,)), user)
            request.POST = {
                    'text': 'text',
                    'title': 'title',
                    'quote': '-1',
                    }
            response = views.new_post(request, obj_id=thread.id)
            self.assertEqual(response.status_code, 302)
        else:
            request = setup_request(self, reverse('forum:thread', args=(thread.id, 0)), user)
            views.thread(request, obj_id=thread.id, page_id=0)
            self.assertEqual(response.status_code, status_code)
    except PermissionDenied:
        self.assertTrue(ass)
    else:
        self.assertFalse(ass)

    # Test file
    if not post:
        post = create_post(self, thread, None)
        attachment = create_attachment(self, post)
        request = setup_request(self, reverse('forum:file', args=(attachment.id,)), user)
        try:
            views.file(request, obj_id=attachment.id)
        except PermissionDenied:
            self.assertTrue(ass)
        else:
            self.assertFalse(ass)

def run_admin_acts(self, level, user_args):
    setup(self)

    # Create user
    user = None
    ua = None
    if user_args:
        if user_args.get('usertype', False):
            ua = user_args['usertype']
            del user_args['usertype']
        if ua == 'admin':
            user = User.objects.get(username='admin')
        else:
            user = create_profile(self, **user_args).user
    else:
        user = AnonymousUser()

    # Create board
    board = create_board(self, exclusive=True)
    ul = None
    if ua:
        if ua == 'no-access':
            ul = 4
        elif ua == 'access':
            board.users.add(user.profile)
            ul = 3
        elif ua == 'moderator':
            board.moderators.add(user.profile)
            ul = 2
        elif ua == 'owner':
            board.owners.add(user.profile)
            ul = 1
        elif ua == 'admin':
            ul = 0

    def empty(**kwargs):
        pass

    request = setup_request(self, '', user)
    try:
        views.check_admin_perms(Board, level)(empty)(request, board.id)
    except PermissionDenied:
        self.assertTrue(ul > level)
    else:
        self.assertTrue(ul <= level)


class check_model_perms_tests(TestCase):
    def test_unauth_public_view(self):
        run_objects(self,
                user_args = None,
                board_args = { 'ass' : False },
                thread_args = { 'ass' : False },
                )

    def test_unauth_private_view(self):
        run_objects(self,
                user_args = None,
                board_args = {
                    'ass' : False,
                    'exclusive' : True,
                    'private' : True
                    },
                thread_args = {
                    'ass' : False,
                    'private' : True
                    },
                status_code = 302
                )

    def test_admin_private_view(self):
        run_objects(self,
                user_args = {
                    'admin' : True
                    },
                board_args = {
                    'ass' : False,
                    'exclusive' : True,
                    'private' : True
                    },
                thread_args = {
                    'ass' : False,
                    'private' : False
                    }
                )

    def test_auth_view_n_bp_tp(self):
        run_objects(self,
                user_args = {
                    'usertype': 'no-access'
                    },
                board_args = {
                    'ass' : True,
                    'exclusive' : True,
                    'private': True,
                    },
                thread_args = {
                    'ass': True,
                    'private': True
                    }
                )

    def test_auth_view_n_bp_tu(self):
        run_objects(self,
                user_args = {
                    'usertype': 'no-access'
                    },
                board_args = {
                    'ass': True,
                    'exclusive': True,
                    'private': True,
                    },
                thread_args = {
                    'ass': True,
                    'private': False
                    }
                )

    def test_auth_view_n_bu_tp(self):
        run_objects(self,
                user_args = {
                    'usertype': 'no-access'
                    },
                board_args = {
                    'ass': False,
                    'exclusive': True,
                    'private': False
                    },
                thread_args = {
                    'ass': True,
                    'private': True
                    }
                )

    def test_auth_view_a_bp_tp(self):
        run_objects(self,
                user_args = {
                    'usertype': 'access'
                    },
                board_args = {
                    'ass': False,
                    'exclusive': True,
                    'private': True
                    },
                thread_args = {
                    'ass': False,
                    'private': True
                    }
                )

    def test_auth_view_a_d(self):
        run_objects(self,
                user_args = {
                    'usertype': 'access'
                    },
                board_args = {
                    'ass': False,
                    },
                thread_args = {
                    'ass': True,
                    'deleted': True
                    }
                )

    def test_unauth_post(self):
        run_objects(self,
                user_args = None,
                board_args = {
                    'ass': False,
                    },
                thread_args = {
                    'ass': False
                    },
                post = True
                )

    def test_auth_post_n(self):
        run_objects(self,
                user_args = {
                    'usertype': 'no-access'
                    },
                board_args = {
                    'ass': False,
                    'exclusive': True
                    },
                thread_args = {
                    'ass': True
                    },
                post = True
                )

    def test_auth_post_na(self):
        run_objects(self,
                user_args = {
                    'usertype': 'no-access'
                    },
                board_args = {
                    'ass': False,
                    },
                thread_args = {
                    'ass': False
                    },
                post = True
                )

    def test_auth_post_b(self):
        run_objects(self,
                user_args = {
                    'usertype': 'banned'
                    },
                board_args = {
                    'ass': False
                    },
                thread_args = {
                    'ass': True
                    },
                post = True
                )

class check_admin_perms_tests(TestCase):
    def test_levels(self):
        uls = ('no-access', 'access', 'moderator', 'owner', 'admin')
        for ul in uls:
            for l in range(0, 3):
                run_admin_acts(self, l, { 'usertype': ul })


class views_index_tests(TestCase):
    def test_index_unauth(self):
        setup(self)

        pb = create_board(self)
        rb = create_board(self, exclusive=True, private=True)

        req = setup_request(self, reverse('forum:index'), AnonymousUser())
        resp = views.index(req)
        self.assertContains(resp, pb.name)
        self.assertNotContains(resp, rb.name)
        self.assertNotContains(resp, 'New board', html=True)
        self.assertNotContains(resp, 'Delete', html=True)

    def test_index_auth(self):
        setup(self)

        us = create_profile(self)

        pb = create_board(self)
        rb = create_board(self, exclusive=True, private=True)
        ab = create_board(self, exclusive=True, private=True)

        ab.users.add(us)

        req = setup_request(self, reverse('forum:index'), us.user)
        resp = views.index(req)
        self.assertContains(resp, pb.name)
        self.assertContains(resp, ab.name)
        self.assertNotContains(resp, rb.name)
        self.assertNotContains(resp, 'New board', html=True)
        self.assertNotContains(resp, 'Delete', html=True)

    def test_index_admin(self):
        setup(self)

        us = Profile.objects.get(user__username='admin')

        pb = create_board(self)
        rb = create_board(self, exclusive=True, private=True)

        req = setup_request(self, reverse('forum:index'), us.user)
        resp = views.index(req)
        self.assertContains(resp, pb.name)
        self.assertContains(resp, rb.name)
        self.assertContains(resp, 'New board', html=True)
        self.assertContains(resp, 'Delete', html=True)

class views_board_tests(TestCase):
    def setup(self, ul, view):
        user = AnonymousUser()
        if ul:
            if ul == 'admin':
                user = Profile.objects.get(user__username='admin').user
            else:
                user = create_profile(self).user

        board = create_board(self, exclusive=True)
        if ul:
            if ul == 'user':
                board.users.add(user.profile)
            elif ul == 'moderator':
                board.moderators.add(user.profile)
            elif ul == 'owner':
                board.moderators.add(user.profile)

        pt = create_thread(self, board)
        rt = create_thread(self, board, private=True)
        dt = create_thread(self, board, deleted=True)

        req = setup_request(self, reverse('forum:board', args=(board.id, 1)), user)
        resp = views.board(req, obj_id=board.id, page_id=1)

        if view['pt']:
            self.assertContains(resp, pt.title)
        else:
            self.assertNotContains(resp, pt.title)

        if view['rt']:
            self.assertContains(resp, rt.title)
        else:
            self.assertNotContains(resp, rt.title)

        if view['dt']:
            self.assertContains(resp, dt.title)
        else:
            self.assertNotContains(resp, dt.title)

    def test_board_thread_list(self):
        setup(self)
        uls = ('admin', 'owner', 'moderator', 'user', 'none')
        for i in range(0, 5):
            self.setup(uls[i], view = { 'pt': True, 'rt': i < 4, 'dt': i < 3, })

class views_thread_tests(TestCase):
    def setup(self, ul, view):
        user = AnonymousUser()
        if ul:
            if ul == 'admin':
                user = Profile.objects.get(user__username='admin').user
            else:
                user = create_profile(self).user

        board = create_board(self, exclusive=True)
        if ul:
            if ul == 'user':
                board.users.add(user.profile)
            elif ul == 'moderator':
                board.moderators.add(user.profile)
            elif ul == 'owner':
                board.moderators.add(user.profile)

        thread = create_thread(self, board)
        op = create_profile(self)

        pp = create_post(self, thread, op)
        dp = create_post(self, thread, op, deleted=True)
        qd = create_post(self, thread, op, quote=dp)

        req = setup_request(self, reverse('forum:thread', args=(thread.id, 1)), user)
        resp = views.thread(req, obj_id=thread.id, page_id=1)

        if view['pp']:
            self.assertContains(resp, pp.text)
        else:
            self.assertNotContains(resp, pp.text)

        if view['dp']:
            self.assertContains(resp, dp.text, count=2)
        else:
            self.assertNotContains(resp, dp.text, msg_prefix="ul = " + ul)

    def test_thread_post_list(self):
        setup(self)
        uls = ('admin', 'owner', 'moderator', 'user', 'none')
        for i in range(0, 5):
            self.setup(uls[i], view = { 'pp': True, 'dp': i < 3, })

class models_modifiers(TestCase):
    def check_atr(self, user, obj, name, atr, atr_name='', no_page=False):
        if not atr_name:
            atr_name = atr
        v = eval("obj.{}".format(atr))

        if no_page:
            req = setup_request(self, reverse('forum:{}_{}'.format(name, atr_name), args=(obj.id,)), user)
            exec("views.{}_{}(req, obj_id=obj.id)".format(name, atr_name))
        else:
            req = setup_request(self, reverse('forum:{}_{}'.format(name, atr_name), args=(obj.id, 0)), user)
            exec("views.{}_{}(req, obj_id=obj.id, page_id=0)".format(name, atr_name))
        obj = type(obj).objects.get(pk=obj.pk)
        nv = eval("obj.{}".format(atr))
        self.assertNotEqual(v, nv)

    def test_board(self):
        setup(self)
        user = Profile.objects.get(user__username='admin').user
        board = create_board(self)

        attr = {
                'exclusive' : 'exc',
                'allow_files' : 'files',
                'private' : ''
                }
        for a in attr:
            self.check_atr(user=user, obj=board, name="board", atr=a, atr_name=attr[a])

        request = setup_request(self, reverse('forum:board_delete', args=(board.id,)), user)
        views.board_delete(request, obj_id=board.id)
        self.assertFalse(Board.objects.filter(pk=board.id).exists())

    def test_thread(self):
        setup(self)
        user = Profile.objects.get(user__username='admin').user
        board = create_board(self, exclusive=True)
        thread = create_thread(self, board)

        attr = {
                'sticky' : '',
                'deleted' : 'delete',
                'locked' : 'lock',
                'private' : '',
                }

        for a in attr:
            self.check_atr(user=user, obj=thread, name="thread", atr=a, atr_name=attr[a])

        request = setup_request(self, reverse('forum:thread_destroy', args=(thread.id, 0)), user)
        views.thread_destroy(request, obj_id=thread.id, page_id=0)
        self.assertFalse(Thread.objects.filter(pk=thread.id).exists())

    def test_post(self):
        setup(self)
        user = Profile.objects.get(user__username='admin').user
        board = create_board(self, exclusive=True)
        thread = create_thread(self, board)
        post = create_post(self, thread, user.profile)

        attr = {
                'sticky' : '',
                'deleted' : 'delete'
                }

        for a in attr:
            self.check_atr(user=user, obj=post, name="post", atr=a, atr_name=attr[a], no_page=True)

        request = setup_request(self, reverse('forum:post_destroy', args=(post.id, 0)), user)
        views.post_destroy(request, obj_id=post.id, page_id=0)
        self.assertFalse(Post.objects.filter(pk=post.id).exists())
