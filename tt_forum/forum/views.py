from django.shortcuts import render, get_object_or_404, redirect
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login
from django.urls import reverse
from django.http import HttpResponse
from django.db import transaction
from django.db.models import Q, F
from django.db.models.functions import Lower
from django.core.exceptions import PermissionDenied
from django.core.paginator import Paginator
from django.core.mail import send_mail
from django.conf import settings
from django.template import loader
from math import ceil
from re import findall, search

from .models import Board, Thread, Post, Profile, Attachment

def check_model_perms(obj_type, post=False):
    def dec(fun):
        def inner(request, obj_id, **kwargs):
            obj = get_object_or_404(obj_type.objects.select_related(), pk=obj_id)

            # Get thread and board and profile applicable to object
            th = None
            if not obj_type is Board:
                th = obj
                while not type(th) is Thread:
                    th = th.parent
            br = th.parent if th else obj

            prf = Profile.objects.get(user=request.user) if request.user.is_authenticated else None

            # Check view permissions
            if not prf or not prf.admin:
                if br.private or (th and th.private) or post:
                    if not prf:
                        return redirect("{}?next={}".format(reverse('login'), request.path))
                    if not br.users.filter(pk=prf.pk).exists():
                        raise PermissionDenied

            # Check modify permissions (users without an unconfirmed email
            # address are not allowed to post)
            if post and not prf.admin:
                if br.banned_users.filter(pk=prf.pk).exists() or not prf.user.email:
                    raise PermissionDenied
            return fun(request=request, obj=obj, br=br, prf=prf, **kwargs)
        return inner
    return dec

# Every view that uses check_admin_perms requires atomicity
# and locking of the objects they use so the whole check_admin_perms
# function is made a transaction block and the applicable objects are
# locked with select_for_update
#
# Level 0: admin
# Level 1: board-owner
# Level 2: moderator
def check_admin_perms(obj_type, level):
    def dec(fun):
        @transaction.atomic
        def inner(request, obj_id, **kwargs):
            # User must be authenticated
            if not request.user.is_authenticated:
                return redirect("{}?next={}".format(reverse('login'), request.path))

            # Get obj and applicable board
            prf = Profile.objects.select_related().get(user=request.user)
            obj = get_object_or_404(obj_type.objects.select_related().select_for_update(), pk=obj_id)

            br = obj
            while not type(br) is Board:
                br = br.parent

            # Check permissions level
            ul = 3
            if prf.admin:
                ul = 0
            elif br.owners.filter(pk=prf.pk).exists():
                ul = 1
            elif br.moderators.filter(pk=prf.pk).exists():
                ul = 2

            if ul > level:
                raise PermissionDenied

            return fun(request=request, obj=obj, br=br, prf=prf, level=ul, **kwargs)
        return inner
    return dec

# Level 0: admin
# Level 1: board-owner
# Level 2: moderator
# Level 3: normal-user
# Level 4: not authenticated/no access to board
# Level 5: banned-user
# Level 6: user has no confirmed email
def get_user_level(br, prf):
    level = 3
    if prf is None:
        level = 4
    elif prf.admin:
        level = 0
    elif not prf.user.email:
        level = 6
    elif br.owners.filter(pk=prf.pk).exists():
        level = 1
    elif br.moderators.filter(pk=prf.pk).exists():
        level = 2
    elif br.banned_users.filter(pk=prf.pk).exists():
        level = 5
    elif not br.users.filter(pk=prf.pk).exists():
        level = 4
    return level

def send_confirm_email(prf, req):
    tpl = loader.get_template('forum/email/confirm_email.html')
    context = { 'token' : prf.gen_token() }
    send_mail(
            subject='Please confirm your COS Forum email address',
            html_message=tpl.render(context, req),
            from_email='no-reply@cosforum.com',
            recipient_list=[prf.new_em],
            message='')

def paginate(request, objs, page_id):
    pp = request.session.get('objs_pp', 10)
    page = Paginator(objs, pp)
    if not 1 <= page_id <= page.num_pages:
        page_id = page.num_pages
    return (page.page(page_id), page_id, page.num_pages)

def validate_pword(pword):
    if getattr(settings, "DEBUG_PWORD", False):
        return None
    if len(pword) < 8:
        return "len"
    if not search(r'[a-z]', pword):
        return "lower"
    if not search(r'[A-Z]', pword):
        return "upper"
    if not search(r'[0-9]', pword):
        return "number"
    if not search(r'[^a-zA-Z0-9]', pword):
        return "special"
    return None

def index(request):
    br = None
    prf = None
    if not request.user.is_authenticated:
        br = Board.objects.filter(private=False)
    else:
        prf = request.user.profile
        if not prf.admin:
            br = Board.objects.filter(Q(private=False) | Q(exclusive=False) |
                    Q(users=prf)).distinct()
        else:
            br = Board.objects.all()
    br = br.order_by(Lower('name'))
    context = { 'boards': br, 'profile' : prf }
    return render(request, 'forum/index.html', context)

@check_model_perms(Board)
def board(request, obj, prf, page_id, **kwargs):
    level = get_user_level(obj, prf)

    # If board is not exclusive and user does not have access, add user
    if level == 4 and prf and not obj.exclusive:
        obj.users.add(prf)
        level = 3

    args = {}
    if level > 2:
        args['deleted'] = False
        if level > 3:
            args['private'] = False

    th = obj.children.filter(**args).order_by('-sticky', '-last')
    th, page_id, max_pages = paginate(request, th, page_id)

    subbed = obj.subbed_users.filter(pk=prf.id).exists() if prf else False

    context = {
            'board' : obj,
            'threads' : th,
            'page_id' : page_id,
            'page_max' : max_pages,
            'level' : level,
            'subbed' : subbed
            }
    return render(request, 'forum/board.html', context)

@check_model_perms(Thread)
def thread(request, obj, br, prf, page_id, **kwargs):
    level = get_user_level(br, prf)
    if obj.deleted and level > 2:
        raise PermissionDenied

    posts = obj.children.all().prefetch_related().order_by('-sticky', 'time')
    posts, page_id, max_pages = paginate(request, posts, page_id)
    context = {
            'board' : br,
            'thread' : obj,
            'posts' : posts,
            'page_id' : page_id,
            'page_max' : max_pages,
            'level' : level
            }
    return render(request, 'forum/thread.html', context)

def post(request, obj_id):
    pst = get_object_or_404(Post.objects.select_related(), pk=obj_id)
    st = pst.parent.children.all().order_by('-sticky', 'time')
    pos = ceil((list(st).index(pst) + 1) / int(request.session.get('objs_pp', 10)))

    return redirect(reverse('forum:thread', args=(pst.parent.id, pos)) +
            "#post_" + str(pst.id))

# sendfile can be used to optimise serving of files
@check_model_perms(Attachment)
def file(request, obj, br, prf, **kwargs):
    if (obj.parent.parent.deleted or obj.parent.deleted) \
            and get_user_level(br, prf) > 2:
                raise PermissionDenied

    # For testing purposes
    if not obj.attachment:
        return HttpResponse("No file")
    import magic
    ft = magic.from_file("{}/{}".format(settings.ATTACHMENT_ROOT, obj.attachment.name), mime=True)
    response = HttpResponse(obj.attachment.file, content_type=ft)
    response['Content-Disposition'] = 'attachment; filename="{}"'.format(obj.name)
    return response

@login_required
def profile(request, obj_id):
    cur_prf = Profile.objects.get(user=request.user)
    prf = get_object_or_404(Profile, pk=obj_id)

    own = Profile.objects.none()
    mod = own

    if cur_prf.admin:
        own = Board.objects.all()
    else:
        own = cur_prf.owned_boards.all()
        mod = cur_prf.mod_boards.all()

    cmb = (own | mod).distinct().order_by(Lower('name'))
    if not cmb:
        raise PermissionDenied

    boards = []
    for b in cmb:
        bn = b.banned_users.filter(pk=prf.id).exists()
        o = not bn and b.owners.filter(pk=prf.id).exists()
        m = not bn and (o or b.moderators.filter(pk=prf.id).exists())
        a = not bn and (o or m or not b.exclusive or b.users.filter(pk=prf.id).exists())
        cown = b in own
        boards.append({
                'id' : b.id,
                'name' : b.name,
                'exc' : b.exclusive,
                'own' : o,
                'mod' : m,
                'acc' : a,
                'ban' : bn,
                'cown' : cown
                })

    context = {
            'boards' : boards,
            'profile' : prf,
            'cur_prf' : cur_prf
            }
    return render(request, 'forum/profile.html', context)

@login_required
def new_board(request):
    prf = Profile.objects.get(user=request.user)
    if not prf.admin:
        raise PermissionDenied

    name = request.POST.get('name', '').strip()
    if not name:
        raise PermissionDenied

    Board.objects.create(name=name)

    return redirect(reverse('forum:index'))

@check_model_perms(Board, post=True)
def new_thread(request, obj, prf, **kwargs):
    title = request.POST.get('title', '').strip();
    text = request.POST.get('text', '').rstrip();
    if not text or not title:
        raise PermissionDenied

    prf.posts = F('posts') + 1
    prf.save()

    th = Thread.objects.create(title=title, parent=obj)
    if prf.notf_auto:
        th.subbed_users.add(prf)

    np = Post.objects.create(user=prf, text=text, parent=th)

    obj.last = np.time
    obj.save()

    su = obj.subbed_users.select_related().all()
    if su:
        context = { 'thread' : th }
        tpl = loader.get_template('forum/email/sub_board.html').render(context, request)
        name = obj.name

        for usr in su:
            m = usr.user.email
            if m:
                send_mail(subject='New thread in \"' + name + '\"',
                        html_message=tpl,
                        from_email='no-reply@cosforum.com',
                        recipient_list=[m],
                        message='')

    return redirect(reverse('forum:thread', args=(th.id, 0)))

@check_model_perms(Thread, post=True)
def new_post(request, obj, br, prf, **kwargs):
    text = request.POST.get('text', '').rstrip()

    if not text:
        raise PermissionDenied

    # Check if thread is locked
    if obj.locked and get_user_level(br, prf) > 2:
        raise PermissionDenied

    # Update poster profile
    prf.posts = F('posts') + 1
    prf.save()

    notf = []

    # Get relevant quote info
    qt = None
    qv = int(request.POST.get('quote', -1))
    if qv >= 0:
        try:
            qt = Post.objects.select_related().get(pk=qv)
        except Post.DoesNotExist:
            raise PermissionDenied
        if qt.parent != obj:
            raise PermissionDenied
        notf.append(qv)

    np = Post.objects.create(user=prf, text=text, parent=obj, quote=qt)

    # Find all references to other posts
    for ref in findall(r'@[0-9]+@', text):
        notf.append(ref[1:-1])

    # Attach files
    if br.allow_files and request.FILES.get('attachments', None):
        for f in request.FILES.getlist('attachments'):
            Attachment.objects.create(attachment=f, parent=np, name=f.name)

    # Update thread/board
    obj.last = np.time
    obj.save()
    br.last = np.time
    br.save()

    # Notify all mentioned users
    if notf:
        tpl = loader.get_template('forum/email/post_rq.html').render({'post' : np}, request)

        q = None
        for nt in set(notf):
            q = (q | Q(pk=nt)) if q else Q(pk=nt)
        notf = Post.objects.filter(q).values_list('user', flat=True)

        for nt in set(notf):
            if nt == prf.pk:
                continue
            try:
                nt = Profile.objects.get(pk=nt)
            except Profile.DoesNotExist:
                continue
            if nt.notf_qr and nt.user.email:
                context = { 'post' : np }
                send_mail(subject='Your post has been mentioned',
                        html_message=tpl,
                        from_email='no-reply@cosforum.com',
                        recipient_list=[nt.user.email],
                        message='')

    # Notify all subbed users
    su = obj.subbed_users.select_related().all()
    if su:
        context = { 'thread' : obj, 'post' : np }
        tpl = loader.get_template('forum/email/sub_thread.html').render(context, request)
        title = obj.title
        for usr in su:
            m = usr.user.email
            if m:
                send_mail(subject='New post in \"' + title + '\"',
                        html_message=tpl,
                        from_email='no-reply@cosforum.com',
                        recipient_list=[m],
                        message='')

    # Add user to sublist if auto_sub is turned on
    if prf.notf_auto:
        obj.subbed_users.add(prf)
        obj.save()

    return redirect(reverse('forum:thread', args=(obj.id, 0)))

@check_admin_perms(Board, level=1)
def board_exc(request, obj, page_id, **kwargs):
    f = not obj.exclusive
    obj.exclusive = f
    if not f:
        obj.private = False
        obj.children.all().update(private=False)
    obj.save()

    return redirect(reverse('forum:board', args=(obj.id, page_id)))

@check_admin_perms(Board, level=1)
def board_private(request, obj, page_id, **kwargs):
    if not obj.exclusive:
        raise PermissionDenied
    pr = not obj.private
    obj.private = pr
    if pr:
        users = obj.users.all()
        for usr in set(obj.subbed_users.all()).difference(users):
            obj.subbed_users.remove(usr)
        for th in obj.children.all():
            for usr in set(th.subbed_users.all()).difference(users):
                th.subbed_users.remove(usr)

    obj.save()
    return redirect(reverse('forum:board', args=(obj.id, page_id)))

@check_admin_perms(Board, level=1)
def board_files(request, obj, page_id, **kwargs):
    obj.allow_files = not obj.allow_files
    obj.save()

    return redirect(reverse('forum:board', args=(obj.id, page_id)))

@check_admin_perms(Board, level=0)
def board_delete(request, obj, **kwargs):
    obj.delete()
    return redirect(reverse('forum:index'))

@check_model_perms(Board)
def board_sub(request, obj, prf, page_id, **kwargs):
    if obj.subbed_users.filter(pk=prf.id).exists():
        obj.subbed_users.remove(prf)
    else:
        obj.subbed_users.add(prf)
    return redirect(reverse('forum:board', args=(obj.id, page_id)))

@check_admin_perms(Thread, level=2)
def thread_sticky(request, obj, page_id, **kwargs):
    obj.sticky = not obj.sticky
    obj.save()
    return redirect(reverse('forum:board', args=(obj.parent.id, page_id)))

@check_admin_perms(Thread, level=2)
def thread_delete(request, obj, page_id, **kwargs):
    obj.deleted = not obj.deleted
    obj.save()

    return redirect(reverse('forum:board', args=(obj.parent.id, page_id)))

@check_admin_perms(Thread, level=2)
def thread_lock(request, obj, page_id, **kwargs):
    obj.locked = not obj.locked
    obj.save()

    return redirect(reverse('forum:board', args=(obj.parent.id, page_id)))

@check_admin_perms(Thread, level=2)
def thread_private(request, obj, br, page_id, **kwargs):
    if not obj.parent.exclusive:
        raise PermissionDenied
    pr = not obj.private
    obj.private = pr

    if pr:
        users = br.users.all()
        for usr in set(obj.subbed_users.all()).difference(users):
            obj.subbed_users.remove(usr)
    obj.save()

    return redirect(reverse('forum:board', args=(obj.parent.id, page_id)))

@check_admin_perms(Thread, level=0)
def thread_destroy(request, obj, br, page_id, **kwargs):
    for post in obj.children.select_related().all():
        post.user.posts = F('posts') - 1
        post.user.save()

    obj.delete()
    return redirect(reverse('forum:board', args=(br.id, page_id)))

@check_model_perms(Thread)
def thread_sub(request, obj, page_id, prf, **kwargs):

    if obj.subbed_users.filter(pk=prf.id).exists():
        obj.subbed_users.remove(prf)
    else:
        obj.subbed_users.add(prf)
    return redirect(reverse('forum:board', args=(obj.parent.id, page_id)))

@check_admin_perms(Post, level=2)
def post_sticky(request, obj, **kwargs):
    obj.sticky = not obj.sticky
    obj.save()

    return redirect(reverse('forum:thread', args=(obj.parent.id, 1)))

@check_admin_perms(Post, level=2)
def post_delete(request, obj, **kwargs):
    obj.deleted = not obj.deleted
    obj.save()

    return redirect(reverse('forum:post', args=(obj.id,)))

@check_admin_perms(Post, level=0)
def post_destroy(request, obj, page_id, **kwargs):
    th = obj.parent.id
    obj.parent.posts = F('posts')-1
    obj.parent.save()
    obj.user.posts =  F('posts')-1
    obj.user.save()
    obj.delete()

    return redirect(reverse('forum:thread', args=(th, page_id)))

@check_admin_perms(Board, level=0)
def profile_own(request, obj, profile_id, **kwargs):
    prf = get_object_or_404(Profile, pk=profile_id)
    if obj.owners.filter(pk=profile_id).exists():
        obj.owners.remove(prf)
    else:
        obj.banned_users.remove(prf)
        obj.owners.add(prf)
        obj.moderators.remove(prf)
        obj.users.add(prf)

    return redirect(reverse('forum:profile', args=(profile_id,)))

@check_admin_perms(Board, level=1)
def profile_mod(request, obj, profile_id, **kwargs):
    prf = get_object_or_404(Profile, pk=profile_id)
    cur = obj in prf.mod_boards.all()
    if obj.moderators.filter(pk=profile_id).exists():
        obj.moderators.remove(prf)
    else:
        obj.banned_users.remove(prf)
        obj.owners.remove(prf)
        obj.moderators.add(prf)
        obj.users.add(prf)

    return redirect(reverse('forum:profile', args=(profile_id,)))

@check_admin_perms(Board, level=2)
def profile_acc(request, obj, profile_id, **kwargs):
    prf = get_object_or_404(Profile, pk=profile_id)
    if obj.users.filter(pk=profile_id).exists():
        obj.users.remove(prf)

        pr = obj.private
        if pr:
            obj.subbed_users.remove(prf)
        for th in obj.children.all():
            if pr or th.private:
                th.subbed_users.remove(prf)
    else:
        obj.banned_users.remove(prf)
        obj.users.add(prf)

    return redirect(reverse('forum:profile', args=(profile_id,)))

@check_admin_perms(Board, level=2)
def profile_ban(request, obj, profile_id, **kwargs):
    prf = get_object_or_404(Profile, pk=profile_id)
    if obj.banned_users.filter(pk=profile_id).exists():
        obj.banned_users.remove(prf)
    else:
        obj.owners.remove(prf)
        obj.moderators.remove(prf)
        obj.users.remove(prf)
        obj.banned_users.add(prf)

    return redirect(reverse('forum:profile', args=(profile_id,)))

@login_required
def profile_search(request):
    username = request.POST.get('username', '').strip()
    if not username:
        raise PermissionDenied
    prf = get_object_or_404(Profile, user__username=username)
    return redirect(reverse('forum:profile', args=(prf.id,)))

@check_admin_perms(Board, level=2)
def profile_bulk(request, obj, prf, page_id, **kwargs):
    level = get_user_level(obj, prf)
    set_to = int(request.POST.get('level', -1))

    if set_to <= level:
        raise PermissionDenied

    if not request.FILES.get('csv', None):
        raise PermissionDenied

    names = [t.strip() for t in request.FILES['csv'].read().decode('ascii').split(',')]
    for name in names:
        prf = None
        try:
            prf = Profile.objects.get(user__username=name)
        except Profile.DoesNotExist:
            continue
        (obj.users.add if set_to <= 3 else obj.users.remove)(prf)
        (obj.moderators.add if set_to == 2 else obj.moderators.remove)(prf)
        (obj.owners.add if set_to == 1 else obj.owners.remove)(prf)

    return redirect(reverse('forum:board', args=(obj.id, page_id)))

def register(request):
    if request.user.is_authenticated:
        return redirect(reverse('forum:index'))

    if request.method != 'POST':
        return render(request, 'registration/register.html',
                request.session.get('register', None))

    def check(name):
        v = request.POST.get(name, '').strip()
        if not v:
            raise PermissionDenied
        return v

    def red(err):
        request.session['register']['error'][err] = True
        return redirect(reverse('forum:register'))

    request.session['register'] = { 'prev' : {}, 'error': {}}
    u = request.session['register']['prev']['username'] = check('username')
    e = request.session['register']['prev']['email'] = check('email')
    p = request.session['register']['prev']['password'] = check('password')
    c = request.session['register']['prev']['password2'] = check('password2')

    if p != c:
        return red('password_match')
    if Profile.objects.filter(user__username=u):
        return red('username')
    if Profile.objects.filter(user__email=e):
        return red('email')
    err = validate_pword(p)
    if err:
        return red('password_inv_{}'.format(err))

    user = User.objects.create_user(
            username=u,
            password=p,
            is_superuser=False,
            is_staff=False)
    user.save()
    prf = Profile(user=user, new_em=e)
    prf.save()
    send_confirm_email(prf, request)

    del request.session['register']
    return redirect(reverse('forum:register_success'))

def register_success(request):
    return render(request, 'registration/register_success.html', None)

@login_required
def my_account(request):
    if request.method != 'POST':
        context ={ 'prf' : Profile.objects.get(user=request.user) }
        context.update(request.session.get('my_account', {}))
        try:
            del request.session['my_account']
        except KeyError:
            pass
        return render(request, 'forum/my_account.html', context)

    def err(name):
        v = request.POST.get(name, '').strip()
        if not v:
            raise PermissionDenied
        return v

    def red(name):
        request.session['my_account']['error'][name] = True
        return redirect(reverse('forum:my_account'))

    def suc(name):
        request.session['my_account'] = { 'success' : { name: True } }
        return redirect(reverse('forum:my_account'))

    request.session['my_account'] = {
            'prev': {},
            'error': {},
            }
    sect = request.POST.get('section', '').strip()

    if sect == 'ppic':
        f = request.FILES.get('new_pp', None)
        if not f:
            raise PermissionDenied
        import imghdr
        if not imghdr.what('', h=f.read()):
            return red('pp_inv')

        prf = Profile.objects.get(user=request.user)
        prf.prf_pic = f
        prf.save()
        return suc('ppic')
    elif sect == 'email':
        email = err('email')

        if Profile.objects.filter(user__email=email):
            return red('email_taken')

        prf = Profile.objects.get(user=request.user)
        prf.new_em = email
        prf.cnf_em = False
        prf.save()
        send_confirm_email(prf, request)
        return suc('email')
    elif sect == 'password':
        cpass = err('cpassword')
        npass = err('password')
        spass = err('password2')

        if npass != spass:
            return red('password_match')
        if not authenticate(username=request.user.username, password=cpass):
            return red('password_cinv')
        e = validate_pword(npass)
        if e:
            return red('password_inv_{}'.format(e))

        request.user.set_password(npass)
        request.user.save()
        login(request, request.user)
        return suc('password')
    elif sect == 'notifications':
        notf = request.POST.getlist('notf')
        prf = Profile.objects.get(user=request.user)

        prf.notf_qr = 'quote' in notf
        prf.notf_auto = 'auto_sub' in notf

        prf.save()
    else:
        raise PermissionDenied

    return redirect(reverse('forum:my_account'))

@login_required
def confirm_email(request, token):
    with transaction.atomic():
        prf = Profile.objects.select_related().select_for_update().get(user=request.user)
        if prf.cnf_em:
            return redirect(reverse('forum:index'))
        suc = prf.val_token(token)
        if suc:
            prf.cnf_em = True
            prf.user.email = prf.new_em
            prf.new_em = ''
            prf.user.save()
            prf.save()

    context = {
           'prf' : prf,
           'success' : suc
           }

    return render(request, 'forum/confirm_success.html', context)
